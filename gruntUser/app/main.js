/**
 * Created by leilihuang on 15/7/3.
 */
(function(){
    var obj = document.getElementById('requirejs'),
        baseJsUrl =obj&& obj.getAttribute('data-url')?obj.getAttribute('data-url') : '/',
        loadJsUrl =obj&& obj.getAttribute('data-main'),
        slot = baseJsUrl.match(/[a-zA-Z]\d/),
        isDebug = 0;
    //获取js路径

    if(loadJsUrl){

    }


    if(slot && slot.length>0){
        slot =  slot[0];
        baseJsUrl = baseJsUrl.match(/http:\/\/[\w\.]*\//)[0];
    }

    function getBaseJsUrl(url){
        //return baseJsUrl+url;
        return "http://s.zzt.tm/portal-static/js/"+url;
    }

    var config = {
        paths: {
            jQuery: [
                getBaseJsUrl('jquery')
            ],
            layout: [
                getBaseJsUrl('jquery.lazyload')
            ]
        },
        shim: {
            jQuery: {
                deps: [],
                exports: '$'
            },
            layout: {
                deps: ['jQuery'],
                exports: 'Layout'
            }
        }
    };

    require.config(config);
})();