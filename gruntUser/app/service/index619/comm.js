define(
    ['jQuery', 'lib/ui/class'],
    function ($, Class) {
        var comm = Class.create({
            setOption: function (opts) {
                var options = {}
                $.extend(true, options, opts);
            }
        }, {
            init: function (opts) {
                this.setOption(opts);
                this.bindEvent();
                this.TagHover(".tip-show-btn", ".tip-show", 35, 70);
                this.initFuction();
                this.MenuWidth();
                this.sTab(".nav_cart", ".nav_cart_sub");//�������ﳵtab
                this.TimeNav(".nav_sort_a", ".nav_sort_sub_a", ".navtaghd_a", ".navtagbd_a");
                this.TimeNav(".nav_sort_b", ".nav_sort_sub_b", ".navtaghd_b", ".navtagbd_b");
                this.NavTagHover(".navtaghd_a", ".navtagbd_a");
                this.NavTagHover(".navtaghd_b", ".navtagbd_b");
                this.scrollEvent();
                this.DivHeight(".sitebar-out", 0);
                this.DivHeight(".sitebar", 0);
                this.DivHeight(".sitebar-box", 0);
                this.DivHeight(".sitebar-con-bd", 35);
            },
            scrollEvent: function () {
                var nav_top = $(".nav"), ah = 0;
                var nav_topH = $(".nav").height();
                var haederH = $(".header").height();

                function scrollMenu(){
                    var yy = $(this).scrollTop();
                    if ($(".banner-spread").css("display") == "block") {
                        ah = $(".banner-spread").height()
                    } else {
                        ah = 0
                    }
                    var minY = haederH + ah + 46;
                    if ($(".detail_tit").length <= 0) {
                        if (yy >= minY) {
                            $(".nav-box").slideDown();
                        } else {
                            $(".nav-box").slideUp();
                        }
                    }
                    if ($(".left-bar").length > 0) {
                        if ((yy >= minY)) {
                            $(".left-bar").show()
                        } else {
                            $(".left-bar").hide()
                        }
                    }
                    if ($(".right-bar").length > 0) {
                        if ((yy >= minY)) {
                            $(".right-bar").show()
                        } else {
                            $(".right-bar").hide()
                        }
                    }
                }
                $(window).scroll(function () {
                    var topScroll=$(this).scrollTop();
                    if (navigator.userAgent.indexOf("MSIE 6.0") > 0 || navigator.userAgent.indexOf("MSIE 7.0") > 0) {
                          setTimeout(function(){
                              var scrollTop=$(window).scrollTop();
                              if(topScroll==scrollTop){
                                  scrollMenu();
                              }
                          },1000);
                    }else{
                        scrollMenu();
                    }

                })
            },
            TimeNav: function (onhover, hoverbox, taghd, tagbd) {
                var sTime;
                $(onhover).hover(
                    function () {
                        clearTimeout(sTime);
                        sTime = setTimeout(function () {
                            $(hoverbox).show();
                        }, 100);
                    },
                    function () {
                        clearTimeout(sTime);
                        sTime = setTimeout(function () {
                            $(hoverbox).hide();
                            $(taghd).removeClass("cur");
                            $(tagbd).addClass("hide");
                        }, 300);
                    }
                );
            },
            NavTagHover: function (taghdH, tagbdH) {
                var k = $(taghdH).length;
                for (var i = 0; i < k; i++) {
                    $(taghdH).eq(i).attr("rel", i)
                }

                $(taghdH).hover(function () {
                    var rel = $(this).attr("rel");
                    $(this).addClass("cur");
                    $(this).siblings().removeClass("cur");
                    $(tagbdH).eq(rel).removeClass("hide");
                    $(tagbdH).eq(rel).siblings(tagbdH).addClass("hide");
                })

            },
            tabs: function (tabId, tabNum) {
                $(tabId + " .tab a").removeClass("cur");
                $(tabId + " .tab a").eq(tabNum).addClass("cur");
                $(tabId + " .tabcon").hide();
                $(tabId + " .tabcon").eq(tabNum).show();
            },
            initFuction: function () {
                var _this = this;

                function CloseDiv(Closebtnn, Closeboxx) {
                    $(Closebtnn).live("click", function () {
                        $(Closeboxx).css({
                            "display": "none"
                        })
                    })
                }

                function DivMargin(DivMarginclass) {
                    if ($(DivMarginclass).length > 0) {
                        $(DivMarginclass).eq(0).css({
                            "margin-top": $(window).height() * 0.18 + "px"
                        });
                        $(window).resize(function () {
                            $(DivMarginclass).eq(0).css({
                                "margin-top": $(window).height() * 0.18 + "px"
                            })
                        })
                    }
                }

                DivMargin(".sitebar-tab01");

                function CloseBtn(closebtn, sitebarbox, sitebarall) {
                    $(closebtn).live("click", function () {
                        var boxwidth = $(this).parents(sitebarbox).width();
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: -boxwidth + "px"
                        }, 300, function () {
                            $(sitebarbox).css({
                                "visibility": "hidden",
                                "z-index": "999997"
                            });
                            _this.MenuWidth();
                        })
                    })
                }

                CloseBtn(".sitebar-box-close", ".sitebar-box", ".sitebar");

                function GotoTop(topbtn) {
                    function scrollT(){
                        if ($(window).scrollTop() > 0) {
                            $(topbtn).show(500)
                        } else {
                            $(topbtn).hide(500)
                        }
                    }
                    if ($(topbtn).length > 0) {
                        $(window).scroll(function () {
                            var topScroll=$(this).scrollTop();
                            if (navigator.userAgent.indexOf("MSIE 6.0") > 0 || navigator.userAgent.indexOf("MSIE 7.0") > 0) {
                                setTimeout(function(){
                                    var scrollTop=$(window).scrollTop();
                                    if(topScroll==scrollTop){
                                        scrollT();
                                    }
                                },1000);
                            }else{
                                scrollT();
                            }

                        });
                        $(topbtn).on("click", function () {
                            $("body,html").animate({
                                scrollTop: 0
                            }, 500)
                        })
                    }
                }

                GotoTop(".sitebar-tab09");

                function SpaceClick(sitebarallclass, sitebarall, sitebarbox, sitebarnumb) {
                    if ($(sitebarall).length > 0) {
                        $(document).live("click", function (e) {
                            var target = $(e.target);
                            if (target.hasClass(sitebarallclass) || target.parents().hasClass(sitebarallclass)) {
                                return
                            } else {
                                if ($(sitebarall).css("right") == "0px") {
                                    var sitebarwid = $(sitebarall).width();
                                    var siteconwid = $(sitebarall).width() - sitebarnumb;
                                    $(sitebarall).animate({
                                        right: -siteconwid + "px"
                                    }, 300, function () {
                                        $(sitebarbox).css({
                                            "visibility": "hidden",
                                            "z-index": "999997"
                                        });
                                        _this.MenuWidth();
                                    })
                                }
                            }
                        })
                    }
                }

                function SiteBar(sitebarbtn, sitebarcon, sitebarnumb, sitebarall) {
                    var sitebarallclass = $(sitebarall).attr("class");
                    var siteconwid = $(sitebarcon).width();
                    var siteallwid = siteconwid + sitebarnumb;
                    $(sitebarall).width(siteallwid + "px");
                    if ($(sitebarcon).css("visibility") == "hidden") {
                        $(sitebarcon).css({
                            "visibility": "visible",
                            "z-index": "999998"
                        });
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: "0"
                        }, 300)
                    } else {
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: -siteconwid + "px"
                        }, 300, function () {
                            $(sitebarcon).css({
                                "visibility": "hidden",
                                "z-index": "999997"
                            });
                            _this.MenuWidth();
                        })
                    }
                }

                function SiteBarShow(sitebarbtn, sitebarcon, sitebarnumb, sitebarall, sitebarbox) {
                    var siteconwid = $(sitebarcon).width();
                    var siteallwid = siteconwid + sitebarnumb;
                    $(sitebarall).width(siteallwid + "px");
                    if ($(sitebarcon).css("visibility") == "hidden") {
                        var siteconheig = $(sitebarbox).height();
                        $(sitebarbox).css({
                            "visibility": "hidden",
                            "z-index": "999997"
                        });
                        $(sitebarcon).css({
                            "visibility": "visible",
                            "z-index": "999998",
                            "top": siteconheig + "px"
                        });
                        $(sitebarcon).stop(false, false);
                        $(sitebarcon).animate({
                            top: "0"
                        }, 300)
                    } else {
                        $(sitebarall).stop(false, false);
                        $(sitebarall).animate({
                            right: -siteconwid + "px"
                        }, 300, function () {
                            $(sitebarcon).css({
                                "visibility": "hidden",
                                "z-index": "999997"
                            });
                            _this.MenuWidth();
                        })
                    }
                }

                function Sitebaranimate(sitebarbtn, sitebarcon, sitebarnumb, sitebarall, sitebarbox) {
                    $(sitebarbtn).live("click", function () {

                        var siteconwid = -($(sitebarall).width() - sitebarnumb);
                        if ($(sitebarall).css("right") == "0px") {
                            SiteBarShow(sitebarbtn, sitebarcon, sitebarnumb, sitebarall, sitebarbox)
                        }
                        if ($(sitebarall).css("right") == siteconwid + "px") {
                            SiteBar(sitebarbtn, sitebarcon, sitebarnumb, sitebarall)
                        }

                    })
                }


                function UnLogin(sitebarbtn, sitebarlogbox, sitebarlogclass, sitebarallclass, closebtn) {
                    if ($(sitebarlogbox).length > 0) {
                        $(sitebarbtn).live("click", function () {
                            var windowsH = $(window).height();
                            var sitebaralln = "." + sitebarallclass;
                            if ($(sitebaralln).attr("class").indexOf("site-min-height") > 0) {
                                var sitebarallnH = $(sitebaralln).height();
                                if (windowsH < sitebarallnH) {
                                    windowsH = sitebarallnH
                                }
                            }
                            var sitebarbtntop = $(this).offset().top - $(sitebaralln).offset().top - 1;
                            var bottomH = windowsH - sitebarbtntop;
                            var sitebarlogheight = $(sitebarlogbox).height();
                            var siteH = $(this).height();
                            if (bottomH > sitebarlogheight) {
                                $(sitebarlogbox).css({
                                    "display": "block",
                                    "top": sitebarbtntop + "px",
                                    "bottom": "auto"
                                });
                                $(sitebarlogbox).removeClass("curbottom")
                            } else {
                                $(sitebarlogbox).css({
                                    "display": "block",
                                    "top": "auto",
                                    "bottom": bottomH - siteH + "px"
                                });
                                $(sitebarlogbox).addClass("curbottom")
                            }
                            if ($(sitebarlogbox).css("display") == "block") {
                                $(document).live("mouseover", function (e) {
                                    var target = $(e.target);
                                    if (target.hasClass(sitebarallclass) || target.parents().hasClass(sitebarallclass) || target.hasClass(sitebarlogclass)) {
                                        return
                                    } else {
                                        $(sitebarlogbox).css({
                                            "display": "none"
                                        })
                                    }
                                })
                            }
                        });
                        $(closebtn).live("click", function () {
                            $(this).parents(sitebarlogbox).css({
                                "display": "none"
                            })
                        })
                    }
                }

                function SiteBarOne() {
                    SpaceClick("sitebar", ".sitebar", ".sitebar-box", 35);
                    Sitebaranimate(".sitebar-tab01", ".sitebar-con01", 35, ".sitebar", ".sitebar-box");
                    Sitebaranimate(".sitebar-tab02", ".sitebar-con02", 35, ".sitebar", ".sitebar-box")
                }

                function SiteBarOne_f() {
                    SpaceClick("sitebar", ".sitebar", ".sitebar-box", 35);
                    Sitebaranimate(".sitebar-tab02", ".sitebar-con02", 35, ".sitebar", ".sitebar-box");
                    CloseDiv(".sitebar-tab02", ".sitebar-login")
                }

                function SiteBarTwo() {
                    UnLogin(".sitebar-tab01", ".sitebar-login", "sitebar-login", "sitebar", ".sitebar-login-close")
                }

                function SiteBarsSH(judge) {
                    if (judge == 0) {
                        SiteBarTwo();
                        SiteBarOne_f()
                    }
                    if (judge == 1) {
                        SiteBarOne()
                    }
                };
                SiteBarsSH(1);
            },
            sTab: function (p, s) {
                var def;
                $(p).hover(
                    function () {
                        clearTimeout(def);
                        def = setTimeout(function () {
                            $(s).show()
                        }, 100);
                    },
                    function () {
                        clearTimeout(def);
                        def = setTimeout(function () {
                            $(s).hide()
                        }, 300);
                    }
                );
            },
            TagHover: function (taghdH, tagbdH, tagnumb01, tagnumb02) {
                $(taghdH).live("hover", function (event) {
                    if (event.type == "mouseenter") {
                        $(this).addClass("cur");
                        if ($(this).children(tagbdH).length > 0) {
                            $(this).children(tagbdH).stop(false, false);
                            $(this).children(tagbdH).css("display", "block");
                            $(this).children(tagbdH).animate({
                                right: tagnumb01 + "px",
                                opacity: "1"
                            }, 300)
                        }
                    } else {
                        $(this).removeClass("cur");
                        if ($(this).children(tagbdH).length > 0) {
                            $(this).children(tagbdH).stop(false, false);
                            $(this).children(tagbdH).animate({
                                right: tagnumb02 + "px",
                                opacity: "0"
                            }, 300, function () {
                                $(this).css("display", "none")
                            })
                        }
                    }
                })
            },
            MenuWidth: function () {
                if ($(window).width() <= 1235 && $(".sitebar").css("right") != "0px") {
                    var aa = $(".sitebar").width();
                    $(".sitebar").animate({
                        right: "-" + aa + "px"
                    }, 500);
                    $(".sitebar-fix").animate({
                        left: "-35px"
                    }, 500);
                    $(".sitebar-out").addClass("icon-hover");
                    $(".icon-hover").live("hover", function (event) {
                        if (event.type == "mouseenter") {
                            if (!$(".sitebar").is(":animated") && $(".sitebar").css("right") != "0px") {
                                var aa = $(".sitebar").width() - 35;
                                $(".sitebar").animate({
                                    right: "-" + aa + "px"
                                }, 500);
                                $(".sitebar-fix").animate({
                                    left: "0px"
                                }, 500)
                            }
                        } else {
                            if (!$(".sitebar").is(":animated") && $(".sitebar").css("right") != "0px") {
                                var aa = $(".sitebar").width();
                                $(".sitebar").animate({
                                    right: "-" + aa + "px"
                                }, 500);
                                $(".sitebar-fix").animate({
                                    left: "-35px"
                                }, 500)
                            }
                        }
                    })
                } else {
                    var aa = $(".sitebar").width() - 35;
                    $(".sitebar").animate({
                        right: "-" + aa + "px"
                    }, 500);
                    $(".sitebar-fix").animate({
                        left: "0px"
                    }, 500);
                    $(".sitebar-out").removeClass("icon-hover")
                }
            },
            DivHeight: function (DivClass, snumb) {
                if ($(DivClass).length > 0) {
                    $(DivClass).height($(window).height() - snumb);
                    $(window).resize(function () {
                        $(DivClass).height($(window).height() - snumb)
                    })
                }
            },
            bindEvent: function () {
                var _this = this;
                if ($(".banner-spread").length > 0 && $(".banner-spread").css("display") == "block") {
                    $(".banner-spread").slideDown();
                    $(".close-x").click(function () {
                        $(".banner-spread").slideUp();
                    })
                }

                $(window).resize(function () {
                    _this.MenuWidth();
                });
            }
        });
        return comm;
    });