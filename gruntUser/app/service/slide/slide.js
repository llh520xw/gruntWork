define(
    ['jQuery','lib/ui/class'],
    function($,Class){
        var slide=Class.create({
            setOptions:function(opts){
                var options={
                    ulId:null,
                    time:1000,
                    btn:null
                }
                $.extend(true,this,options,opts);
            }
        },{
            init:function(opts){
                this.setOptions(opts);
                this.autoScroll();
                this.setTime(this.time);
                this.btnClick();
            },
            autoScroll:function(){
                this.height=this.ulId.height();
                this.ulId.find("li").eq(0).css("top",0);
            },
            setTime:function(time){
                var _this=this;
                setInterval(function(){
                    var first=_this.ulId.find("li").eq(0);
                    var next=first.next("li");
                    var i=next.attr("class");
                    first.animate({top:_this.height},1000);
                    _this.ulId.append(first);
                    next.animate({top:0},1000);
                    _this.btnSelect(i);
                },time);
            },
            btnSelect:function(i){
                this.btn.find(".cur").removeClass("cur");
                this.btn.find("button").eq(i).addClass("cur");
            },
            btnClick:function(){
                var _this=this;
                this.btn.find("button").each(function(i){
                   $(this).on("click",function(){
                       _this.ulId.find("li").eq(0).animate({top:_this.height},1000);
                       var first=_this.ulId.find("."+i);
                       first.animate({top:"0"},1000);
                        _this.slideAry(first);
                       _this.btnSelect(i);
                   });
                });
            },
            slideAry:function(first){
                var d=first.prevUntil(),_this=this;
                for(var i= d.length-1;i>0 ;i--){
                    console.log(d[i]);
                    _this.ulId.append(d[i]);
                }
            }
        })
        return slide;
    }
)