define(
    ['jQuery','lib/ui/class'],
    function($,Class){
        var home=Class.create({
            setOptions:function(opts){
                var options={

                }
                $.extend(true,options,opts);
            }
        },{
            init:function(opts){
                this.setOptions(opts);
                this.initFunction();
                this.bindEvent();
                this.eachUtil();
            },
            initFunction:function(){
                function showHotTime(TimeName) {
                    var TimeLeft = TimeName;
                    var NubTime = TimeLeft.length;
                    var leftTime = new Array();
                    var leftsecond = new Array();
                    var date = new Array();
                    var day1 = new Array();
                    var hour = new Array();
                    var minute = new Array();
                    var second = new Array();
                    var now = new Date();
                    for (i = 0; i < NubTime; i++) {
                        date[i] = new Date($(TimeLeft[i]).attr("data-time"));
                        leftTime[i] = date[i].getTime() - now.getTime();
                        leftsecond[i] = parseInt(leftTime[i] / 1000);
                        if (leftsecond[i] > 0) {
                            day1[i] = Math.floor(leftsecond[i] / (60 * 60 * 24));
                            hour[i] = Math.floor((leftsecond[i] - day1[i] * 24 * 60 * 60) / 3600);
                            minute[i] = Math.floor((leftsecond[i] - day1[i] * 24 * 60 * 60 - hour[i] * 3600) / 60);
                            second[i] = Math.floor(leftsecond[i] - day1[i] * 24 * 60 * 60 - hour[i] * 3600 - minute[i] * 60);
                            if (day1[i] > 0) {
                                $(TimeLeft[i]).html("<b>剩余：" + day1[i] + "天" + hour[i] + "小时</b>")
                            } else {
                                if (hour[i] > 0) {
                                    $(TimeLeft[i]).html("<b>剩余：" + hour[i] + "小时</b>")
                                } else {
                                    if (minute[i] > 0) {
                                        $(TimeLeft[i]).html("<b>剩余：" + minute[i] + "分钟</b>")
                                    } else {
                                        if (second[i] > 0) {
                                            $(TimeLeft[i]).html("<b>剩余：" + second[i] + "秒</b>")
                                        } else {
                                            $(TimeLeft[i]).html("<b>活动已结束</b>")
                                        }
                                    }
                                }
                            }
                        } else {
                            $(TimeLeft[i]).html("<b>活动已结束</b>")
                        }
                    }
                }
                function ShowPre(o) {
                    var that = this;
                    this.box = $("." + o["box"]);
                    this.btnP = $("." + o.Pre);
                    this.btnN = $("." + o.Next);
                    this.v = o.v || 1;
                    this.c = 0;
                    var li_node = "li";
                    this.loop = o.loop || false;
                    if (this.loop) {
                        this.li = this.box.find(li_node);
                        this.box.append(this.li.eq(0).clone(true))
                    }
                    this.li = this.box.find(li_node);
                    this.l = this.li.length;
                    if (this.l <= this.v) {
                        this.btnP.hide();
                        this.btnN.hide()
                    }
                    this.deInit = true;
                    this.w = this.li.outerWidth(true);
                    this.box.width(this.w * this.l);
                    this.maxL = this.l - this.v;
                    this.s = o.s || 1;
                    if (this.s > 1) {
                        this.w = this.v * this.w;
                        this.maxL = Math.floor(this.l / this.v);
                        this.box.width(this.w * (this.maxL + 1));
                        var addNum = (this.maxL + 1) * this.v - this.l;
                        var addHtml = "";
                        for (var adN = 0; adN < addNum; adN++) {
                            addHtml += '<li class="addBox"><div class="photo"></div><div class="text"></div></li>'
                        }
                        zthis.box.append(addHtml)
                    }
                    this.numIco = null;
                    if (o.numIco) {
                        this.numIco = $("." + o.numIco);
                        var numHtml = "";
                        numL = this.loop ? (this.l - 1) : this.l;
                        for (var i = 0; i < numL; i++) {
                            numHtml += '<a href="javascript:void(0);">' + i + "</a>"
                        }
                        this.numIco.html(numHtml);
                        this.numIcoLi = this.numIco.find("a");
                        this.numIcoLi.bind("click", function() {
                            if (that.c == $(this).html()) {
                                return false
                            }
                            that.c = $(this).html();
                            that.move()
                        })
                    }
                    this.bigBox = null;
                    this.loadNumBox = null;
                    if (o.loadNumBox) {
                        this.loadNumBox = $("#" + o.loadNumBox)
                    }
                    this.allNumBox = null;
                    if (o.loadNumBox) {
                        this.allNumBox = $("#" + o.allNumBox);
                        if (o.bBox) {
                            var cAll = this.l < 10 ? ("0" + this.l) : this.l
                        } else {
                            var cAll = this.maxL < 10 ? ("0" + (this.maxL + 1)) : (this.maxL + 1)
                        }
                        this.allNumBox.html(cAll)
                    }
                    if (o.bBox) {
                        this.bigBox = $("#" + o.bBox);
                        this.li.each(function(n) {
                            $(this).attr("num", n);
                            var cn = (n + 1 < 10) ? ("0" + (n + 1)) : n + 1;
                            $(this).find(".text").html(cn)
                        });
                        this.loadNum = 0;
                        this.li.bind("click", function() {
                            if (that.loadNum == $(this).attr("num")) {
                                return false
                            }
                            var test = null;
                            if (that.loadNum > $(this).attr("num")) {
                                test = "pre"
                            }
                            that.loadNum = $(this).attr("num");
                            that.loadImg(test)
                        });
                        that.loadImg();
                        if (o.bNext) {
                            that.bNext = $("#" + o.bNext);
                            that.bNext.bind("click", function() {
                                that.loadNum < that.l - 1 ? that.loadNum++ : that.loadNum = 0;
                                that.loadImg()
                            })
                        }
                        if (o.bPre) {
                            that.bPre = $("#" + o.bPre);
                            that.bPre.bind("click", function() {
                                that.loadNum > 0 ? that.loadNum-- : that.loadNum = that.l - 1;
                                that.loadImg("pre")
                            })
                        }
                    }
                    if (this.loop) {
                        this.btnP.bind("click", function() {
                            if (that.c <= 0) {
                                that.c = that.l - 1;
                                that.box.css({
                                    left: -that.c * that.w
                                })
                            }
                            that.c--;
                            that.move(1)
                        });
                        this.btnN.bind("click", function() {
                            if (that.c >= (that.l - 1)) {
                                that.box.css({
                                    left: 0
                                });
                                that.c = 0
                            }
                            that.c++;
                            that.move(1)
                        })
                    } else {
                        this.btnP.bind("click", function() {
                            that.c > 0 ? that.c-- : that.c = that.maxL;
                            that.move(1)
                        });
                        this.btnN.bind("click", function() {
                            that.c < that.maxL ? that.c++ : that.c = 0;
                            that.move(1)
                        })
                    }
                    that.timer = null;
                    if (o.auto) {
                        that.box.bind("mouseover", function() {
                            clearInterval(that.timer)
                        });
                        that.box.bind("mouseleave", function() {
                            that.autoPlay()
                        });
                        that.autoPlay()
                    }
                    this.move()
                }
                ShowPre.prototype = {
                    move: function(test) {
                        var that = this;
                        var pos = this.c * this.w;
                        if (test && that.timer) {
                            clearInterval(that.timer)
                        }
                        if (that.numIco) {
                            that.numIcoLi.removeClass("on");
                            var numC = that.c;
                            if (that.loop && (that.c == (this.l - 1))) {
                                numC = 0
                            }
                            that.numIcoLi.eq(numC).addClass("on")
                        }
                        this.box.stop();
                        this.box.animate({
                            left: -pos
                        }, function() {
                            if (test && that.auto) {
                                that.autoPlay()
                            }
                            if (that.loop && that.c == that.maxL) {
                                that.c = 0;
                                that.box.css({
                                    left: 0
                                })
                            }
                        });
                        if (that.bigBox) {
                            return false
                        }
                        if (that.loadNumBox) {
                            var loadC = parseInt(that.c) + 1;
                            loadC = loadC < 10 ? "0" + loadC : loadC;
                            that.loadNumBox.html(loadC)
                        }
                    },
                    autoPlay: function() {
                        var that = this;
                        that.timer = setInterval(function() {
                            that.c < that.maxL ? that.c++ : that.c = 0;
                            that.move()
                        }, 5000)
                    }
                };
                if ($(".heros img").length > 1) {
                    var ShowPre1 = new ShowPre({
                        box: "heros",
                        Pre: "prev",
                        Next: "next",
                        numIco: "mask-left",
                        loop: 1,
                        auto: 1
                    })
                }
            },
            bindEvent:function(){
                $("#hnav_fi li").click(function() {
                    var abc = $("a", this).attr("href");
                    var scroll_offset = $(abc).offset();
                    $("body,html").animate({
                        scrollTop: scroll_offset.top
                    }, 500)
                });
                $("#H_go_top").click(function() {
                    scrollTo(0, 0)
                });

                $("#h_banner").hover(function() {
                    $("#index_b_hero .next").css({
                        "display": "block"
                    });
                    $("#index_b_hero .prev").css({
                        "display": "block"
                    })
                }, function() {
                    $("#index_b_hero .next").css({
                        "display": "none"
                    });
                    $("#index_b_hero .prev").css({
                        "display": "none"
                    })
                });
                $(".h_brandlist li").hover(function() {
                    $(this).removeClass("imggray")
                }, function() {
                    $(this).addClass("imggray")
                });
            },
            eachUtil:function(){
                var Util = {
                    getByClass: function(sName, oParent) {
                        var oParent = oParent || document;
                        var results = [];
                        var reg = new RegExp("\\b" + sName + "\\b", i);
                        var objs = oParent.getElementsByTagName("*");
                        for (var i = 0; i < objs.length; i++) {
                            if (reg.test(objs[i].className)) {
                                results.push(objs[i])
                            }
                        }
                        return results
                    },
                    each: function(objs, fn) {
                        for (var i = 0; i < objs.length; i++) {
                            fn(i, objs[i])
                        }
                    }
                };
                var amounts = $(".willbegin_list dd b");
                Util.each(amounts, function(i, amount) {
                    var str = amount.innerHTML;
                    str = str.replace(/\.(\d+)/g, function($1) {
                        return "<i>" + $1 + "</i>"
                    });
                    str = str.replace(/\s/g, "");
                    amount.innerHTML = str
                });
            }
        });

      return home;
    }
)