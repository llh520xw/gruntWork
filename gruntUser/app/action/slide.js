require(
    ['jQuery','service/slide/slide'],
    function($,Slide){
        $(function(){
            new Slide({
                ulId:$("#slide"),
                time:4000,
                btn:$("#btn")
            });
        })
    }
)