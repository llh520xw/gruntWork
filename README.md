# gruntWork
前端自动化环境
学习grunt的同时，自己也学着搭建这套自动化环境，不足之处请多多指点


1：下载node.js 地址https://nodejs.org/ , 安装完成后验证 node -v;

2:下载grunt 安装命令 npm install grunt-cli -g ,详细安装教程http://jingyan.baidu.com/article/93f9803fe354ede0e46f5585.html;

3：在项目目录下执行 npm install 下载环境所需要的grunt插件;

4：sass编译需要配置ruby和sass环境,ruby下载地址:百度一下就有哈,ruby安装详细教程：http://jingyan.baidu.com/article/48b558e33558ac7f38c09aee.html
   sass安装查看sass官网详细教程http://www.w3cplus.com/sassguide/install.html;


PC端  js 点击事件绑定用on去绑定click，尽可能用id元素
H5移动端 js点击事件用on去绑定tap，尽可能用id元素


常用命令
grunt clear              .tmp文件夹下所有内容

grunt css:sass           编译sass样式  //需要在sass.json里面配置，参考例子

grunt css:less           编译less样式  //需要在marge.json里面配置，参考例子

grunt css:min            压缩样式

grunt release:sass       编译压缩样式，拷贝图片到.tmp文件目录下

grunt release:less       编译压缩样式，拷贝图片到.tmp文件目录下

grunt js                 自动合并js   //action为合并js入口 , service下写逻辑，同一个页面逻辑放到同一个文件夹下

grunt jsmin              压缩js

grunt watch:sass         实时监控所有代码，当代码改变时，自动重新编译

grunt watch:less         实时监控所有代码，当代码改变时，自动重新编译

grunt img:min            压缩图片

grunt html:min           压缩html


开发环境带上分支号，默认为s2  路径配置在app/config/jsmin.json文件中
  "slotTest":"",
  "slotDev":"s2/",
  "slotPro":"",

线上环境默认是根目录

开发环境执行
grunt dev  编译js css 拷贝图片到.tmp目录下，替换样式背景图片url路径   //路径配置在app/config/jsmin.json文件中 分别对应static下的devUrl
grunt commit:dev   将.tmp目录下的所有文件放到开发服务器上


测试环境执行
grunt test  编译js css 拷贝图片到.tmp目录下，替换样式背景图片url路径   //路径配置在app/config/jsmin.json文件中 分别对应static下的testUrl
grunt commit:test   将.tmp目录下的所有文件放到测试服务器上


线上环境执行
grunt pro  编译js css 拷贝图片到.tmp目录下，替换样式背景图片url路径   //路径配置在app/config/jsmin.json文件中 分别对应static下的provUrl
grunt commit:pro   将.tmp目录下的所有文件放到开发服务器上，让运维同步更新目录文件，将代码放到线上环境


所有代码默认是提交到 ftp服务器上   

有好的提议可以添加修改
